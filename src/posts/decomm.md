---
title: Decentralized Communication Platforms 
description: Counting on one centralized platform to share information involves trust.
author: Red
date: 2023-03-25
tags:
  - freedom
  - monopoly
  - authority
---

## IRC like

 - matrix.org (via element.io): the Matrix cannot just be shut down by one
   central entity decisions. That is where of most of public discussions happen.

   [Join here https://app.element.io/#/room/#space-dao-community:matrix.org](https://app.element.io/#/room/#space-dao-community:matrix.org)

## Social networks

 - mastodon.world and fosstodon.org are examples of nodes participating in a decentralized social network powered by Mastodon

   [Find Space DAO here https://mastodon.world/@spacedao_ai](https://mastodon.world/@spacedao_ai)


This is a lot to take? It is ok, you only need to take one [Red Pill](https://fosstodon.org/@redsharpbyte). The world
is richer than your environment might let you think.
These platform are not just decentralized, they are open source and we are
making our best to use means that can be reproduced somewhere else, anywhere
else.
