# Space DAO official website

### Install 

```
https://gitlab.com/parametry-ai/space-dao/frontend/www_spacedao_ai.git
cd www_spacedao_ai/
npm install
```

### One time build 

It will generate the first CSS.
Only run once for each deployment.

```
npm run build
```

### Run Eleventy as live reload

```
npm run start
```

